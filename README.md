# A cleaner way of installing the OpenCL driver from AMDGPU-PRO on Debian

The purpose of this project is to make it easier to install the OpenCL driver from AMDGPU-PRO without messing up the open-source AMDGPU driver provided by your distribution.

This procedure is not supported by either AMD or Debian. If this breaks something, please don't complain to either of them.

With that out of the way...

This worked on my machine, running Debian Sid with a Radeon RX 470. I've successfully run darktable, LuxMark and Blender (GPU rendering in Cycles). I expect this to work with various other graphics cards and Debian-derived distros. If you try this with a different distro, Debian release, or graphics card, please open an issue to let me know if it worked on your machine. I hope to make a table of systems this is known to work on or something, so I'd be interested to know even if it worked fine.

While anything is possible, it is unlikely that this approach will leave you without a working graphics driver. It should be easy to reverse the changes by simply uninstalling the package.

## Installation

You will need AMDGPU-PRO from the AMD site, and the helper package from this project.

1. Install the helper package:
   ```
   # dpkg -i amdgpu-pro-helper_0.1_all.deb
   ```

2. Download AMDGPU-PRO. I can't link directly to the download because they check referers, so go to https://www.amd.com/en/support/kb/release-notes/rn-amdgpu-unified-linux-20-40 and click "Radeon™ Software for Linux® version 20.40 for Ubuntu 20.04.1".

   Alternatively, if you've already read the EULA ;)
   ```
   wget --referer https://www.amd.com/ https://drivers.amd.com/drivers/linux/amdgpu-pro-20.40-1147286-ubuntu-20.04.tar.xz
   ```

   (That's the latest version at time of writing, and the one I've tested. If you're reading this in the future, feel free to go to https://www.amd.com/en/support, download a newer version, and open an issue if it doesn't work).

3. Extract AMDGPU-PRO:
   ```
   $ cd ~/Downloads/
   $ tar -xf amdgpu-pro-20.40-1147286-ubuntu-20.04.tar.xz
   $ cd amdgpu-pro-20.40-1147286-ubuntu-20.04/
   ```
4. Install the appropriate OpenCL ICD for your card. If you don't know which one you need, it's probably OK to just install both.

   Older than Vega 10:
   ```
   # dpkg -i opencl-orca-amdgpu-pro-icd_20.40-1147286_amd64.deb
   # ldconfig
   ```

   Vega 10 and newer (I have not tested this; please let me know if this works for you):
   ```
   # dpkg -i opencl-amdgpu-pro-comgr_20.40-1147286_amd64.deb opencl-amdgpu-pro-icd_20.40-1147286_amd64.deb
   # ldconfig
   ```
    
   opencl-orca-amdgpu-pro-icd_20.40-1147286_i386.deb might be useful if you have a pre-Vega 10 card and a 32-bit OpenCL application. I haven't tried it. Who has 32-bit OpenCL applications?
    
5. Optionally, see if that worked:
   ```
   # apt install clinfo
   $ clinfo -l
   ```
   You should see something like
   ```
   Platform #0: AMD Accelerated Parallel Processing
    `-- Device #0: Ellesmere
   ```
If `clinfo -l` returns with no output, something has gone wrong. Either the driver is not installed properly, or your GPU is not being detected.

## Uninstallation

```
apt purge amdgpu-pro-helper
```

This should also remove the AMD packages, since they depend on the helper package (or rather, the metapackage that it `Provide:`s).

## What this actually does

AMDGPU-PRO is not officially available for Debian. There is an Ubuntu version, but it is distributed as a bundle of packages that all depend on a metapackage (amdgpu-pro-core) that forces you to install the whole thing if you install any of it. This entails replacing basically all userspace components of your graphics driver with the versions AMD ships. I have seen suggestions that one can just install the whole Ubuntu driver bundle on Debian, but last time I tried that on Sid, it made OpenCL work, but broke OpenGL. (So now Blender didn't work for a *different reason*. Great.)

I have also seen instructions to extract the relevant libraries from the AMD packages and install them manually. While this approach does work, I don't like having files that the package manager doesn't know about lying around in system paths.

The helper package in this repository does two things. Firstly, it tells apt that AMD's metapackage does not need to be installed (using a `Provides: ` line), allowing you to install individual AMD packages without pulling in the whole bundle. Secondly, it adds a file to /etc/ld.so.conf.d/ to make the dynamic linker look in the /opt/ directory that AMD's packages install to. In this way, you get working OpenCL, no interference with the distro's OpenGL etc. libraries, and clean uninstall.

## Isn't it a bad idea to install Ubuntu packages on Debian?

In general, yes. However, the OpenCL driver has none of the complicated library dependencies that usually cause problems with such an approach - all it needs is libc6.

## Building the helper package

At some point, I'll work out how to do GitLab releases. Until then, you'll have to just build the package yourself:

```
$ git clone https://gitlab.com/BCMM/amdgpu-opencl-on-debian.git
$ cd amdgpu-opencl-on-debian
$ debuild -us -uc
```

Note that this will put the finished .deb at ../amdgpu-pro-helper_0.1_all.deb, i.e. up one level from the current working directory.
